<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>KMS Bookkeeping :: Home Page</title>
        <meta name="description" content="KMS Bookkeeping Home Page."/>
        <?php include_once( 'includes/common_meta_and_css.php' ); ?>
    </head>
    <body>

        <?php include_once( 'includes/common_header.php' ); ?>

        <div class="row">
            <div class="large-12 small-12 columns">

                <div class="row">
                    <div class="large-12 small-12">
                        <div id="featured" data-orbit>
                            <img src="/templates/main/img/banners/banner_we_got_this.png" alt="slide image">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        
        <div class="row fullWidth blueBarBackground">
            <div class="large-12 columns">

                <div class="row">
                    <div class="large-12">
                        &nbsp;
                    </div>
                </div>

            </div>
        </div>
        
        <br />
        
        <div class="row">
            <div class="large-12 columns">

                <div class="row">
                    <div class="large-12">
                        <h1>About KMS Bookkeeping</h1>
                    </div>
                </div>

            </div>
        </div>
        
        <br />

        <div class="row">
            <div class="large-12 columns">
                <div class="row">

                    <div class="large-4 columns">
                        
                        <h5>WHO ARE WE?</h5>
                        
                        <p>
                            At KMS Bookkeeping it is our goal to provide both individuals and small businesses with affordable, accurate and organized bookkeeping and accounting.  By doing this, we will save you time and money.  We want to be your cost effective alternative to in-house bookkeeping and accounting services.  We want to build a business relationship with you that help you succeed and surpass your goals.  We are a reliable business partner ready and able to answer your questions, and help contribute to your success.  We guarantee a response to your questions and inquiries with 24-48 hours regardless if it is a holiday or weekend.
                        </p>
                        
                    </div>
                    
                    <div class="large-4 columns">
                        
                        <h5>WHAT SOFTWARE DO WE USE?</h5>

                        <p>
                            We use two types of software to keep and maintain your books.  QuickBooks, and now Xero.
                        </p>
                        
                        <h5>WHAT IF I DON'T USE QUICKBOOKS OR XERO?</h5>
                        
                        <p>
                            No problem!  We have made a choice as a company to use QuickBooks, and Xero, but because of experience we can use many types of bookkeeping and accounting software programs.
                        </p>
                        
                    </div>
                    
                    <div class="large-4 columns">
                        
                        <h5>WHO DO WE WORK WITH?</h5>
                        
                        <p>
                            We have experience with many types of businesses.  We have worked for contractors, carpet cleaners, realtors, mortgage brokers, software programming, and web development companies, just to name a few.
                        </p>
                        
                        <p>
                            We also help those who are interested in starting up a new business.  Whether you need help filing for a EIN number, or setting up the proper accounting and bookkeeping systems for your business, we can help.
                        </p>
                        
                        <p>
                            For those who have established businesses, and are interested in changing over to us, we can perform clean up and catch up services to get your business back on track.  Even if you have an in-house bookkeeper, it does not hurt to have a second pair of eyes on your books to ensure that everything is in balance.
                        </p>
                        
                    </div>

                </div>
                
                <div class="row">

                    <div class="large-6 columns">
                        
                        <h5>HOW CAN WE HELP YOU?</h5>
                        
                        <!-- Nest two columns inside! -->
                        <div class="row">
                            
                            <div class="large-6 columns">
                                
                                CONTACT US:<br /><br />
                                1722 W Osborn Rd<br />
                                Phoenix, AZ 85022<br />
                                <a href="mailto:info@kmsbookkeeping.com?subject=I%20am%20interested%20in%20your%20services">info@kmsbookkeeping.com</a><br /><br />

                                <strong>T:</strong> 623-205-3483<br />
                                
                            </div>
                            
                            <div class="large-6 columns">
                                
                                <img src="/templates/main/img/license/dog_on_phone.png">
                                
                            </div>
                            
                        </div>
                        
                    </div>
                    
                    <div class="large-6 columns">
                        
                        <h3>Contact Us</h5>

                        <div id="api_response"></div>

                        <form name="ContactForm" id="ContactForm">
                            <div class="row">
                                <div class="large-12 columns">
                                    <input type="text" id="name" name="name" placeholder="Name" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <input type="text" id="email" name="email" placeholder="Email" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <input type="text" id="subject" name="subject" placeholder="Subject" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <textarea rows="4" id="message" name="message" placeholder="Message"></textarea>
                                </div>
                            </div>
                            <input type="button" id="formSubmit" onClick="contactFormHandler.onSubmitContactForm();" class="button" value="Send">
                        </form>
                        
                    </div>
                    
                </div>
                
            </div>
        </div>

        <?php include_once( 'includes/common_footer.php' ); ?>

        <script src="/templates/main/js/vendor/jquery.js"></script>
        <script src="/templates/main/js/foundation.min.js"></script>
        <script src="/templates/main/js/foundation/foundation.js"></script>
        <script src="/templates/main/js/foundation/foundation.offcanvas.js"></script>
        <script>
            $(document).foundation();

            var doc = document.documentElement;
            doc.setAttribute('data-useragent', navigator.userAgent);
        </script>
        <!--script src="/templates/main/js/vendor/modernizr.js"></script-->
        <script src="/templates/main/js/kmsContactForm.js"></script>
    </body>
</html>