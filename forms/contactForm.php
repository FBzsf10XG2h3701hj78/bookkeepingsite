<h3>Contact Us</h5>

<div id="api_response"></div>

<form name="ContactForm" id="ContactForm">
    <div class="row">
        <div class="large-12 columns">
            <input type="text" id="name" name="name" placeholder="Name" />
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <input type="text" id="email" name="email" placeholder="Email" />
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <input type="text" id="subject" name="subject" placeholder="Subject" />
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <textarea rows="4" id="message" name="message" placeholder="Message"></textarea>
        </div>
    </div>
    <input type="button" id="formSubmit" onClick="contactFormHandler.onSubmitContactForm();" class="button" value="Submit">
</form>