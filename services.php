<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>KMS Bookkeeping :: Home Page</title>
        <meta name="description" content="KMS Bookkeeping Home Page."/>
        <?php include_once( 'includes/common_meta_and_css.php' ); ?>
    </head>
    <body>

        <?php include_once( 'includes/common_header.php' ); ?>

        <div class="row">
            <div class="large-12 small-12 columns">

                <div class="row">
                    <div class="large-12 small-12">
                        <div id="featured" data-orbit>
                            <img src="/templates/main/img/banners/banner_thumbs_up.png" alt="slide image">
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row fullWidth blueBarBackground">
            <div class="large-12 columns">

                <div class="row">
                    <div class="large-12">
                        &nbsp;
                    </div>
                </div>

            </div>
        </div>
        
        <br />
        
        <div class="row">
            <div class="large-12 columns">

                <div class="row">
                    <div class="large-12">
                        <h1>Services</h1>
                    </div>
                </div>

            </div>
        </div>
        
        <br />

        <div class="row">
            <div class="large-12 columns">
                <div class="row">

                    <div class="large-6 columns">
                        
                        <p>
                            When you need your books done right, you want someone with the most experience. KMS Bookkeeping is the business you want to call.
                        </p>
                        
                        <p>
                            From start up to clean up we have the ability to solve almost any bookkeeping issue you may have.
                            We can even house and maintain your QuickBooks file allowing you to concentrate on what you do best and grow your business.
                        </p>

                        <h5>BOOKKEEPING SERVICES</h5>
                        
                        <ul>
                            <li>Acounts Payable</li>
                            <li>Accounts Recievable</li>
                            <li>Bank Reconciliation</li>
                            <li>Billing</li>
                            <li>Check writing</li>
                            <li>Invoicing</li>
                            <li>Payroll (W-2 and 1099)</li>
                        </ul>

                        <h5>REPORTING</h5>
                        
                        <ul>
                            <li>Balance sheet reports</li>
                            <li>Cash flow reports</li>
                            <li>Income Statement reports</li>
                            <li>Profit and Loss</li>
                        </ul>
                        
                        <h5>ADDITIONAL SERVICES</h5>
                        
                        <ul>
                            <li>Clean Up and/or Catch Up Bookkeeping</li>
                            <li>QuickBooks and Excel instruction</li>
                            <li>Cloud Bookkeeping</li>
                            <li>FREE Consultation</li>
                        </ul>
                        
                        <p>
                            <strong><i>Contact us today at 623-205-3483 for a FREE consultation. Or email <a href="mailto:info@kmsbookkeeping.com?Subject=I%20would%20like%20a%20FREE%20consultation">info@kmsbookkeeping.com</a>.</i></strong>
                        <p>
                    </div>

                    <div class="large-1 columns">
                        &nbsp;
                    </div>
                    
                    <div class="large-5 columns">
                        
                        <table style="width:100%;">
                            <thead>
                                <tr class="tableDescription">
                                    <th colspan="2">OUR RATES</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Clean Up/Catch Up</td>
                                    <td>$35/hr</td>
                                </tr>
                                <tr>
                                    <td>Monthly Bookkeeping/Accounting</td>
                                    <td>$250/month</td>
                                </tr>
                                <tr>
                                    <td>Consulting Services</td>
                                    <td>$40/hr</td>
                                </tr>
                            </tbody>
                        </table>
                        
                        <h3>Contact Us</h5>

                        <div id="api_response"></div>

                        <form name="ContactForm" id="ContactForm">
                            <div class="row">
                                <div class="large-12 columns">
                                    <input type="text" id="name" name="name" placeholder="Name" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <input type="text" id="email" name="email" placeholder="Email" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <input type="text" id="subject" name="subject" placeholder="Subject" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <textarea rows="4" id="message" name="message" placeholder="Message"></textarea>
                                </div>
                            </div>
                            <input type="button" id="formSubmit" onClick="contactFormHandler.onSubmitContactForm();" class="button" value="Send">
                        </form>
                        
                    </div>

                </div>
            </div>
        </div>

        <?php include_once( 'includes/common_footer.php' ); ?>

        <script src="/templates/main/js/vendor/jquery.js"></script>
        <script src="/templates/main/js/foundation.min.js"></script>
        <script src="/templates/main/js/foundation/foundation.js"></script>
        <script src="/templates/main/js/foundation/foundation.offcanvas.js"></script>
        <script>
            $(document).foundation();

            var doc = document.documentElement;
            doc.setAttribute('data-useragent', navigator.userAgent);
        </script>
        <!--script src="/templates/main/js/vendor/modernizr.js"></script-->
        <script src="/templates/main/js/kmsContactForm.js"></script>
    </body>
</html>