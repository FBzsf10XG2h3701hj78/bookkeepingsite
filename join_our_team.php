<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>KMS Bookkeeping :: Home Page</title>
        <meta name="description" content="KMS Bookkeeping Home Page."/>
        <?php include_once( 'includes/common_meta_and_css.php' ); ?>
    </head>
    <body>

        <?php include_once( 'includes/common_header.php' ); ?>

        <div class="row">
            <div class="large-12 small-12 columns">

                <div class="row">
                    <div class="large-12 small-12">
                        <div id="featured" data-orbit>
                            <img src="/templates/main/img/banners/banner_join_our_team.png" alt="slide image">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        
        <div class="row fullWidth blueBarBackground">
            <div class="large-12 columns">

                <div class="row">
                    <div class="large-12">
                        &nbsp;
                    </div>
                </div>

            </div>
        </div>
        
        <br />
        
        <div class="row">
            <div class="large-12 columns">

                <div class="row">
                    <div class="large-12">
                        <h1>Join Our Team</h1>
                    </div>
                </div>

            </div>
        </div>
        
        <br />

        <div class="row">
            <div class="large-12 columns">
                <div class="row">

                    <div class="large-6 columns">
                        <h5>JOB BOARD</h5>
                        
                        <p>
                            There are no available positions at this time. Keep checking back. Things
                            change quickly.<br /><br />
                            
                            Or follow us on twitter:<br />
                            <img src="/templates/main/img/social/webicon-twitter.png"><br />
                            <a href="//twitter.com/kmsbookkeeping" target="_blank">@kmsbookkeeping</a>
                        </p>
                    </div>

                    <div class="large-6 columns">
                        
                        <h3>Contact Us</h5>

                        <div id="api_response"></div>

                        <form name="ContactForm" id="ContactForm">
                            <div class="row">
                                <div class="large-12 columns">
                                    <input type="text" id="name" name="name" placeholder="Name" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <input type="text" id="email" name="email" placeholder="Email" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <input type="text" id="subject" name="subject" placeholder="Subject" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <textarea rows="4" id="message" name="message" placeholder="Message"></textarea>
                                </div>
                            </div>
                            <input type="button" id="formSubmit" onClick="contactFormHandler.onSubmitContactForm();" class="button" value="Send">
                        </form>
                        
                    </div>

                </div>
            </div>
        </div>

        <?php include_once( 'includes/common_footer.php' ); ?>

        <script src="/templates/main/js/vendor/jquery.js"></script>
        <script src="/templates/main/js/foundation.min.js"></script>
        <script src="/templates/main/js/foundation/foundation.js"></script>
        <script src="/templates/main/js/foundation/foundation.offcanvas.js"></script>
        <script>
            $(document).foundation();

            var doc = document.documentElement;
            doc.setAttribute('data-useragent', navigator.userAgent);
        </script>
        <!--script src="/templates/main/js/vendor/modernizr.js"></script-->
        <script src="/templates/main/js/kmsContactForm.js"></script>
    </body>
</html>