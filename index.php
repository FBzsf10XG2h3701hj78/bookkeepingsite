<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>KMS Bookkeeping :: Home Page</title>
        <meta name="description" content="KMS Bookkeeping Home Page."/>
        <?php include_once( 'includes/common_meta_and_css.php' ); ?>
    </head>
    <body>

        <?php include_once( 'includes/common_header.php' ); ?>

        <div class="row">
            <div class="large-12 small-12 columns">

                <div class="row">
                    <div class="large-12 small-12">
                        <div id="featured" data-orbit>
                            <img src="/templates/main/img/banners/banner_accounting_desktop_c.png" alt="slide image">
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row fullWidth blueBarBackground">
            <div class="large-12 columns">

                <div class="row">
                    <div class="large-12">
                        &nbsp;
                    </div>
                </div>

            </div>
        </div>

        <br />

        <div class="row">
            <div class="large-12 columns">
                <div class="row">

                    <div class="large-4 columns">

                        <h5>BOOKKEEPING SERVICES THAT FREE UP YOUR TIME</h5>

                        As a business owner or manager, your time is valuable. Our role is to effectively
                        manage your financial transactions in a timely and professional manner.

                    </div>

                    <div class="large-4 columns">

                        <h5>WE PROVIDE A RANGE OF SERVICES</h5>
                        <ul>
                            <li>Income Statement Reports</li>
                            <li>Balance Sheet Reports</li>
                            <li>Cash Flow Reports</li>
                            <li>AP/AR</li>
                            <li>Bank Reconciliation</li>
                            <li>Payroll</li>
                            <li>Invoicing</li>
                            <li>..........<a href="/services.php">and more</a></li>
                        </ul>

                    </div>

                    <div class="large-4 columns">

                        <h3>Contact Us</h5>

                            <div id="api_response"></div>

                            <form name="ContactForm" id="ContactForm">
                                <div class="row">
                                    <div class="large-12 columns">
                                        <input type="text" id="name" name="name" placeholder="Name" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <input type="text" id="email" name="email" placeholder="Email" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <input type="text" id="subject" name="subject" placeholder="Subject" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="large-12 columns">
                                        <textarea rows="4" id="message" name="message" placeholder="Message"></textarea>
                                    </div>
                                </div>
                                <input type="button" id="formSubmit" onClick="contactFormHandler.onSubmitContactForm();" class="button" value="Send">
                            </form>

                    </div>

                </div>
            </div>
        </div>

        <?php include_once( 'includes/common_footer.php' ); ?>

        <script src="/templates/main/js/vendor/jquery.js"></script>
        <script src="/templates/main/js/foundation.min.js"></script>
        <script src="/templates/main/js/foundation/foundation.js"></script>
        <script src="/templates/main/js/foundation/foundation.offcanvas.js"></script>
        <script>
            $(document).foundation();

            var doc = document.documentElement;
            doc.setAttribute('data-useragent', navigator.userAgent);
        </script>
        <!--script src="/templates/main/js/vendor/modernizr.js"></script-->
        <script src="/templates/main/js/kmsContactForm.js"></script>
    </body>
</html>