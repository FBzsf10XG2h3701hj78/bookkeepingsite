window.contactFormHandler = new function()
{
    var m_jqLoadingMask = null,
            m_jqXhr = null,
            m_jqContentArea = null,
            m_jqResponseData = null,
            m_countDownSeconds = null,
            m_faderSpeedMilliseconds = null;

    this.init = function() {
        this.m_jqXhr = null;
        this.m_jqContentArea = $("#ajaxResults");
        this.m_jqResponseData = $("#appMessage");
        this.m_countDownSeconds = 10;
        this.m_faderSpeedMilliseconds = 1000;

        $(function() {
            //
        });
    };

    this.showLoadingMask = function(bShow) {
        if (bShow) {
            $('#appMessage').hide();
            $('#loadMessage').show();
        }
        else {
            $('#appMessage').show();
            $('#loadMessage').hide();
            contactFormHandler.countdownToFadeOut('#appMessage');
        }
    };

    this.onSubmitContactForm = function() {
        var url = '/forms/contactFormReceiver.php';

        this.submitForm(url);
    };

    this.initAPI = function() {
        var result = '';
        contactFormHandler.showLoadingMask(true);
        $('#appMessage').html('');
        $("#ajaxResults").html('');
        $(".response").removeClass("response_success");
        $(".response").removeClass("response_fail");
    };

    this.submitForm = function(url) {
        this.initAPI();
        this.m_jqXhr = $.ajax({
            type: 'POST',
            url: url,
            data: { name: $( '#name' ).val(), email: $( '#email' ).val(), subject: $( '#subject' ).val(), message: $( '#message' ).val() },
            dataType: 'json',
//            contentType: "application/json"
        }).success(function(response, textStatus, xhr) {
            $("#api_response").html();
            $("#api_response").html( response.htmlResponse );
            if( response.status==1 ) {
                $( "#ContactForm :input" ).prop( "disabled", true );
                $( "#ContactForm :textarea" ).prop( "disabled", true );
//                $("#ContactForm #formSubmit").hide();
                $( "#ContactForm #formSubmit" ).addClass( "success" );
                $( "#ContactForm #formSubmit" ).addClass( "disabled" );
                
            }
        }).done(function(data) {
        }).error(function(XMLHttpRequest, textStatus, errorThrown) {
        }).always(function(response, textStatus, xhr) {
            this.m_jqXhr = null;
        });
    };

    this.getUrlVars = function(url) {
        var vars = [], hash;
        var hashes = url.slice(url.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            //vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        //return JSON.stringify( vars );
        return vars;
    };

    this.countdownToFadeOut = function(element) {
        contactFormHandler.m_countDownSeconds = 10;
        contactFormHandler.m_faderSpeedMilliseconds = 1000;
        var timer = setInterval(function() {
            $('#appMessage span').text(contactFormHandler.m_countDownSeconds--);
            if (contactFormHandler.m_countDownSeconds === -1) {
                $('#appMessage').fadeOut('fast');
                clearInterval(timer);
            }
        }, contactFormHandler.m_faderSpeedMilliseconds);
    };

};

contactFormHandler.init();