<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" data-useragent="Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>KMS Bookkeeping :: Home Page</title>
        <meta name="description" content="KMS Bookkeeping Home Page."/>
        <?php include_once( 'includes/common_meta_and_css.php' ); ?>
    </head>
    <body>

        <?php include_once( 'includes/common_header.php' ); ?>

        <div class="row">
            <div class="large-12 small-12 columns">

                <div class="row">
                    <div class="large-12 small-12">
                        <div id="featured" data-orbit>
                            <img src="/templates/main/img/banners/banner_accounting_desktop_c.png" alt="slide image">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        
        <div class="row fullWidth blueBarBackground">
            <div class="large-12 columns">

                <div class="row">
                    <div class="large-12">
                        &nbsp;
                    </div>
                </div>

            </div>
        </div>
        
        <br />
        
        <div class="row">
            <div class="large-12 columns">

                <div class="row">
                    <div class="large-12">
                        <h1>Organizations We Sponsor</h1>
                    </div>
                </div>

            </div>
        </div>
        
        <br />

        <div class="row">
            <div class="large-12 columns">
                <div class="row">

                    <div class="large-8 columns">
                        
                        <a href="//www.azhumane.org" target="_blank"><h5>ARIZONA HUMANE SOCIETY</h5></a>
                        
                        <p>
                            Please spay and neuter your pets. As you can see, we love animals.
                            They are more than pets, they are family.
                        </p>
                        
                        <a href="//www.asba.com" target="_blank"><h5>ARIZONA SMALL BUSINESS ASSOCIATION</h5></a>
                        
                        <p>
                            The ASBA can help your business to grow by providing a wealth
                            of resources.
                        </p>
                        
                        <a href="//www.localfirstaz.com" target="_blank"><h5>LOCAL FIRST ARIZONA</h5></a>
                        
                        <p>
                            Local First Arizona is an organization that promotes the local economy
                            by helping consumers purchase and sell goods and services locally.
                        </p>
                        
                        <a href="//www.xero.com/us" target="_blank"><h5>XERO</h5></a>
                        
                        <p>
                            Xero is a cloud-based accounting software. Best in business for collaboration
                            and record keeping between you and your Bookkeeper or Accountant.
                        </p>
                        
                    </div>

                    <div class="large-4 columns">
                        
                        <h3>Contact Us</h5>

                        <div id="api_response"></div>

                        <form name="ContactForm" id="ContactForm">
                            <div class="row">
                                <div class="large-12 columns">
                                    <input type="text" id="name" name="name" placeholder="Name" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <input type="text" id="email" name="email" placeholder="Email" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <input type="text" id="subject" name="subject" placeholder="Subject" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="large-12 columns">
                                    <textarea rows="4" id="message" name="message" placeholder="Message"></textarea>
                                </div>
                            </div>
                            <input type="button" id="formSubmit" onClick="contactFormHandler.onSubmitContactForm();" class="button" value="Send">
                        </form>
                        
                    </div>

                </div>
            </div>
        </div>

        <?php include_once( 'includes/common_footer.php' ); ?>

        <script src="/templates/main/js/vendor/jquery.js"></script>
        <script src="/templates/main/js/foundation.min.js"></script>
        <script src="/templates/main/js/foundation/foundation.js"></script>
        <script src="/templates/main/js/foundation/foundation.offcanvas.js"></script>
        <script>
            $(document).foundation();

            var doc = document.documentElement;
            doc.setAttribute('data-useragent', navigator.userAgent);
        </script>
        <!--script src="/templates/main/js/vendor/modernizr.js"></script-->
        <script src="/templates/main/js/kmsContactForm.js"></script>
    </body>
</html>