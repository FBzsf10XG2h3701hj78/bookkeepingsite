        <br /><br /><br /><br />

        <div class="row fullWidth footerTopBorder">
            <div class="large-12 columns">

                <div class="row">
                    <div class="large-12">
                        &nbsp;
                    </div>
                </div>

            </div>
        </div>

        <footer class="row fullWidth footerBackground">
            <div class="large-12 columns" style="position: relative;">
                <div class="row">
                    <div class="large-6 columns">
                        <p class="footerText">Copyright &copy; 2014 <a href="http://www.kmscreative.com" target="_new">KMScreative LLC</a. All Rights Reserved.</p>
                    </div><div style="position: absolute; height: 125px;width:291px; margin-top:-136px;margin-left:180px;background-image: url('/templates/main/img/license/dogs_over.png');"></div>
                    <div class="large-6 columns">
                        <ul class="inline-list right">
                            <li><a class="footerText" href="/index.php">Home</a></li>
                            <li><a class="footerText" href="/about_us.php">About</a>
                                <ul>
                                    <li><a class="footerText" href="/about_us.php">About Us</a></li>
                                    <li><a class="footerText" href="/organizations_we_sponsor.php">Organizations We Sponsor</a></li>
                                    <li><a class="footerText" href="/join_our_team.php">Join Our Team</a></li>
                                </ul>
                            </li>
                            <li><a class="footerText" href="/services.php">Services</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        <!-- end on-canvas main content -->

    </section>

    <a class="exit-off-canvas"></a>

    </div>
</div>