<div class="off-canvas-wrap docs-wrap" data-offcanvas>
    <div class="inner-wrap">

        <nav class="tab-bar hide-for-medium-up">
            <section class="left-small">
                <a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
            </section>

            <section class="middle tab-bar-section">
                <a href="/index.php"><img src="/templates/main/img/kmsbookkeeping_logo_sm.png" style="height:95%;"></a>
            </section>
        </nav>

        <aside class="left-off-canvas-menu hide-for-medium-up" style="min-height:1000px">
            <ul class="off-canvas-list">
                <!--li><img src="/templates/main/img/kmsbookkeeping_logo_sm.png"></li-->
                <li><a href="/index.php">Home</a></li>
                <li><a href="/about_us.php">About</a>
                    <ul class="dropdown">
                        <li><a href="/about_us.php">About Us</a></li>
                        <li><a href="/organizations_we_sponsor.php">Organizations We Sponsor</a></li>
                        <li><a href="/join_our_team.php">Join Our Team</a></li>
                    </ul>
                </li>
                <li><a href="/services.php">Services</a></li>
            </ul>
        </aside>

        <section class="main-section">

            <!-- on-canvas main content goes here -->

            <div class="row fullWidth blackBarBackground hide-for-small">
                <div class="large-12 columns">

                    <div class="row">
                        <div class="large-12">
                            &nbsp;
                        </div>
                    </div>

                </div>
            </div>

            <nav class="top-bar hide-for-small" data-topbar>
                <ul class="title-area">

                    <li class="name">
                        <h1>
                            <a href="#">
                                <img src="/templates/main/img/kmsbookkeeping_logo_lg.png">
                            </a>
                        </h1>
                    </li>
                    <li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
                </ul>
                <section class="top-bar-section">
                    <ul class="right">
                        <li class="divider"></li>
                        <li><a href="/index.php">Home</a></li>
                        <li class="divider"></li>
                        <li class="has-dropdown">
                            <a href="/about_us.php">About</a>
                            <ul class="dropdown">
                                <li><a href="/about_us.php">About Us</a></li>
                                <li><a href="/organizations_we_sponsor.php">Organizations We Sponsor</a></li>
                                <li><a href="/join_our_team.php">Join Our Team</a></li>
                            </ul>
                        </li>
                        <li class="divider"></li>
                        <li><a href="/services.php">Services</a></li>
                        <li class="divider"></li>
                    </ul>
                </section>
            </nav>